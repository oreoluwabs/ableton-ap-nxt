import { useCallback, useEffect, useMemo, useState } from "react";

const useIntersection = ({
  root = null,
  rootMargin = "0px",
  threshold = 0,
}) => {
  const [observer, setObserver] = useState(null);
  const [entry, setEntry] = useState(null);
  const [node, setNode] = useState(null);

  useEffect(() => {
    observer?.disconnect();

    setObserver(new IntersectionObserver(([entry]) => setEntry(entry)), {
      root,
      rootMargin,
      threshold,
    });

    if (node) observer?.observe(node);

    return () => observer?.disconnect();
  }, [node, root, rootMargin, threshold]);

  // const inView = useMemo(() => {
  //   if (entry) {
  //     return entry.isIntersecting;
  //   }
  // }, [entry]);

  const inView = entry?.isIntersecting;

  const ref = useCallback((e) => {
    setNode(e);
  }, []);

  return { ref, inView, entry };
};

export default useIntersection;
