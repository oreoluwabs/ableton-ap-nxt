import Link from "next/link";
import { useState } from "react";
import styles from "../styles/components/header.module.scss";

const LeftNav = ({ isMenuOpen, setIsMenuOpen }) => {
  return (
    <nav className={styles["nav"]}>
      <ul>
        <li>
          <Link href="/">
            <a>Live</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a>Push</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a>Link</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a>Shop</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a>Packs</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a>Help</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a
              className={`${styles["more_btn"]} ${
                isMenuOpen ? styles["is-open"] : ""
              }`}
              onClick={(e) => {
                e.preventDefault();
                setIsMenuOpen(!isMenuOpen);
              }}
            >
              More
            </a>
          </Link>
        </li>
      </ul>
    </nav>
  );
};
const RightNav = () => {
  return (
    <nav className={styles["nav"]}>
      <ul>
        <li>
          <Link href="/">
            <a>Try Live for free</a>
          </Link>
        </li>
        <li>
          <Link href="/">
            <a>Log in or register</a>
          </Link>
        </li>
      </ul>
    </nav>
  );
};
const MoreOnAbletonNav = () => {
  return (
    <>
      <nav className={`${styles["nav"]} ${styles["more-on-ableton-nav"]}`}>
        <h3>More on Ableton.com:</h3>
        <ul>
          <li>
            <Link href="/">
              <a>Blog</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>Ableton for the Classroom</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>Ableton for the Colleges and Universities</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>Certified Training</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>About Ableton</a>
            </Link>
          </li>
          <li>
            <Link href="/">
              <a>Jobs</a>
            </Link>
          </li>
        </ul>
      </nav>
    </>
  );
};
const MoreFromAbletonNav = () => {
  return (
    <>
      <nav className={`${styles["nav"]} ${styles["more-from-ableton-nav"]}`}>
        <h3>More from Ableton:</h3>
        <div className={styles["slideable-wrapper"]}>
          <div className={styles["slideable"]}>
            <ul>
              <li>
                <div>
                  <Link href="/">
                    <a>
                      <h4>Loop</h4>
                      <p>
                        Watch Talks, Performances and Features from Ableton's
                        Summit for Music Makers
                      </p>
                    </a>
                  </Link>
                </div>
              </li>
              <li>
                <div>
                  <Link href="/">
                    <a>
                      <h4>Learning Music</h4>
                      <p>
                        Learn the fundamentals of music making right in your
                        browser.
                      </p>
                    </a>
                  </Link>
                </div>
              </li>
              <li>
                <div>
                  <Link href="/">
                    <a>
                      <h4>Learning Synths</h4>
                      <p>
                        Get started with synthesis using a web-based synth and
                        accompanying lessons.
                      </p>
                    </a>
                  </Link>
                </div>
              </li>
              <li>
                <div>
                  <Link href="/">
                    <a>
                      <h4>Making Music</h4>
                      <p>
                        Some tips from 74 Creative Strategies for Electronic
                        Producers.
                      </p>
                    </a>
                  </Link>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

const Header = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <header className={styles["header"]}>
      <div className={styles["header-primary"]}>
        <div
          className={`${styles["logo-mobile_menu-group"]} ${
            isMenuOpen ? styles["is-open"] : ""
          }`}
        >
          <svg
            className={styles["logo-mobile_menu-group__logo__image"]}
            version="1.1"
            id="Layer_1"
            xmlns="http://www.w3.org/2000/svg"
            // xmlns:xlink="http://www.w3.org/1999/xlink"
            // xml:space="preserve"
            x="0px"
            y="0px"
            width="45px"
            height="21px"
            viewBox="0 0 45 21"
            enableBackground="new 0 0 45 21"
          >
            <g>
              <rect width="3" height="21"></rect>
              <rect x="6" width="3" height="21"></rect>
              <rect x="12" width="3" height="21"></rect>
              <rect x="18" width="3" height="21"></rect>
              <g>
                <rect x="24" y="18" width="21" height="3"></rect>
                <rect x="24" y="12" width="21" height="3"></rect>
                <rect x="24" y="6" width="21" height="3"></rect>
                <rect x="24" width="21" height="3"></rect>
              </g>
            </g>
          </svg>
          <div
            className={styles["menu_btn"]}
            onClick={() => setIsMenuOpen(!isMenuOpen)}
          >
            Menu
          </div>
        </div>

        <div
          className={`${styles["menu"]} ${isMenuOpen ? styles["is-open"] : ""}`}
        >
          <div className={`${styles["left-nav"]} ${styles["dir-nav"]}`}>
            <LeftNav {...{ isMenuOpen, setIsMenuOpen }} />
          </div>

          <div className={`${styles["right-nav"]} ${styles["dir-nav"]}`}>
            <RightNav />
          </div>

          <div className={styles["more_information"]}>
            <MoreOnAbletonNav />
            <MoreFromAbletonNav />
          </div>
        </div>
      </div>

      <div
        className={`${styles["desktop-more"]} ${
          isMenuOpen ? styles["is-open"] : ""
        }`}
      >
        <div className={styles["more_information"]}>
          <MoreOnAbletonNav />
          <MoreFromAbletonNav />
        </div>
      </div>
    </header>
  );
};

export default Header;
