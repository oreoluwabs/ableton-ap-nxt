import Image from "next/image";
import Link from "next/link";
import { useState } from "react";
// import styles from "../../styles/components/about/content.module.scss";
import aboutStyles from "../../styles/components/about/about.module.scss";

const CollageBlockOne = ({
  extraClasses,
  background,
  position,
  images = [],
}) => {
  const extraC = extraClasses?.map((item) => aboutStyles[item]).join(" ");

  return (
    <div className={`${aboutStyles["collage"]} ${extraC}`}>
      <div
        className={`${aboutStyles["collage-bg"]} ${
          aboutStyles[`collage-bg-${background}`]
        }`}
        data-background={background}
        data-position={position}
      />

      {images.map((item, index) => (
        <div
          key={item.imgSrc + index}
          className={aboutStyles["collage-media"]}
          data-src={item.imgSrc}
          style={{
            backgroundImage: `url("${item.imgSrc}")`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "50% 50%",
          }}
        />
      ))}
    </div>
  );
};

const CollageBlockTwo = ({
  extraClasses,
  background,
  position,
  images = [],
}) => {
  const extraC = extraClasses?.map((item) => aboutStyles[item]).join(" ");

  return (
    <div className={`${aboutStyles["collage"]} ${extraC}`}>
      <div
        className={`${aboutStyles["collage-bg"]} ${
          aboutStyles[`collage-bg-${background}`]
        }`}
        data-background={background}
        data-position={position}
      />
      <div className={aboutStyles["collage-container"]}>
        {images.slice(0, 2).map((item, index) => (
          <div
            key={item.imgSrc + index}
            className={aboutStyles["collage-media"]}
            data-src={item.imgSrc}
            style={{
              backgroundImage: `url("${item.imgSrc}")`,
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "50% 50%",
            }}
          />
        ))}
      </div>

      {images[2] && (
        <div
          key={images[2].imgSrc + 1}
          className={aboutStyles["collage-media"]}
          data-src={images[2].imgSrc}
          style={{
            backgroundImage: `url("${images[2].imgSrc}")`,
            backgroundSize: "cover",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "50% 50%",
          }}
        />
      )}
    </div>
  );
};

const ContentComponent = () => {
  const [isThumbnailClicked, setIsThumbnailClicked] = useState(false);
  return (
    <section>
      {/* Image */}
      <CollageBlockOne
        background="yellow-stone"
        position="right"
        images={[
          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-1.jpg?fit=crop",
          },
          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-2.jpg?fit=crop",
          },
        ]}
      />

      <div className={aboutStyles["text"]}>
        <h2>
          Making music isn’t easy. It takes time, effort, and learning. But when
          you’re in the flow, it’s incredibly rewarding.
        </h2>
        <p>
          We feel the same way about making Ableton products. The driving force
          behind Ableton is our passion for what we make, and the people we make
          it for.
        </p>
      </div>

      {/* video */}
      <div className={aboutStyles["media"]}>
        {!isThumbnailClicked && (
          <>
            <div
              className={aboutStyles["video--wrapper"]}
              onClick={() => {
                setIsThumbnailClicked(true);
              }}
            >
              <Image
                className={aboutStyles["video--poster"]}
                data-src="https://ableton-production.imgix.net/about/poster-juanpe.jpg?fit=crop&amp;auto=format&amp;fm=jpg"
                src="https://ableton-production.imgix.net/about/poster-juanpe.jpg?auto=format&amp;dpr=2&amp;fit=crop&amp;fm=jpg&amp;ixjsv=1.1.3&amp;q=50&amp;w=800"
                width={800}
                height={450}
              />
            </div>
            <p className={aboutStyles["media__caption"]}>
              Why Ableton - Juanpe Bolivar
            </p>
          </>
        )}
        {isThumbnailClicked && (
          <div className={aboutStyles["video--iframe--wrapper"]}>
            <iframe
              width={800}
              height={450}
              id="9sbnhgjeyxa"
              src="//www.youtube-nocookie.com/embed/9SbnhgjeyXA?wmode=transparent&amp;controls=1&amp;vq=hd1080&amp;rel=0&amp;showinfo=0&amp;autohide=1&amp;color=white&amp;modestbranding=1&amp;enablejsapi=1&amp;autoplay=1"
              wmode="transparent"
              frameBorder="0"
              mozAllowFullScreen
              webkitAllowFullScreen
              allowFullScreen
              data-gtm-yt-inspected-61466_128="true"
            ></iframe>
          </div>
        )}
      </div>

      <div className={aboutStyles["text"]}>
        <h2>
          We are more than 350 people from 30 different countries divided
          between our headquarters in Berlin and our offices in Los Angeles and
          Tokyo.
        </h2>
        <p>
          Most of us are active musicians, producers, and DJs, and many of us
          use Live and Push every day. We come from a wide range of cultural and
          professional backgrounds. Some of us have PhDs, some are self-taught,
          and most of us are somewhere in between. What connects us is the
          shared belief that each of us has the skills and knowledge to
          contribute to something big: helping to shape the future of music
          culture.
        </p>
      </div>

      {/* Image */}
      <CollageBlockTwo
        extraClasses={["collage--people"]}
        type="3-box"
        background="lean-green"
        position="left"
        images={[
          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-3.jpg?fit=crop",
          },

          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-4.jpg?fit=crop",
          },
          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-5.jpg?fit=crop",
          },
        ]}
      />

      <div className={aboutStyles["text"]}>
        <h2>
          We believe it takes focus to create truly outstanding instruments. We
          only work on a few products and we strive to make them great.
        </h2>
        <p>
          Rather than having a one-size-fits-all process, we try to give our
          people what they need to work their magic and grow. We’ve learned that
          achieving the best results comes from building teams that are richly
          diverse, and thus able to explore problems from a wider set of
          perspectives. We don’t always agree with each other, but opinion and
          debate are valued and openly encouraged.
        </p>
      </div>

      {/* Image */}
      <div className={`${aboutStyles["media"]} ${aboutStyles["media--large"]}`}>
        <Image
          alt=""
          data-src="https://ableton-production.imgix.net/about/poster-meet-the-makers.jpg?fit=crop&amp;auto=format&amp;fm=jpg"
          src="https://ableton-production.imgix.net/about/poster-meet-the-makers.jpg?auto=format&amp;fit=crop&amp;fm=jpg&amp;ixjsv=1.1.3&amp;w=1334"
          width={1500}
          height={760}
        />
      </div>

      <div className={aboutStyles["text"]}>
        <h2>
          We’re passionate about what we do, but we’re equally passionate about
          improving who we are.
        </h2>
        <p>
          We work hard to foster an environment where people can grow both
          personally and professionally, and we strive to create a wealth of
          opportunities to learn from and with each other.
        </p>
        <p>
          Alongside an internal training program, employees are actively
          supported in acquiring new knowledge and skills, and coached on
          applying these in their daily work. In addition, staff-organized
          development and music salons are a chance to discuss new technologies,
          production techniques and best practices.
        </p>
      </div>

      {/* Image */}
      <CollageBlockOne
        extraClasses={["collage--learning"]}
        background="purple-pain"
        position="center"
        images={[
          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-6-a.jpg?fit=crop",
          },
          {
            imgSrc:
              "https://ableton-production.imgix.net/about/photo-7.jpg?fit=crop",
          },
        ]}
      />

      <div className={aboutStyles["text"]}>
        <h2>
          We want our employees to love it here. Since we’re looking for
          exceptional talent from around the world, we will do everything we can
          to make your transition as easy as possible.
        </h2>
        <p>
          If you're joining us in Berlin, we'll help with relocation and
          paperwork. We’ll even provide you with free German or English lessons.
          Plus, working in Germany means you can expect comprehensive health
          insurance for you and your family, as well as generous maternity and
          paternity leave. Office hours are flexible, but it’s not all work; we
          have several company and team outings throughout the year as well as a
          variety of fun, informal small-group activities.
        </p>
      </div>

      <div className={aboutStyles["job-info"]}>
        <div className={aboutStyles["job-info-img"]}></div>
        <div className={`${aboutStyles["job-info-details"]} bg-washedup-blue`}>
          <div className={aboutStyles["job-content"]}>
            <div className={aboutStyles["body-text"]}>
              <h2>
                We’re really proud of the work we’ve done so far. But there’s so
                much more to come. If you’d like to be a part of it, please join
                us.
              </h2>
              <Link href="/">
                <a>See latest jobs</a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContentComponent;
