import ActiveLink from "../activeLink";

import styles from "../../styles/components/about/stickynav.module.scss";
import useIntersection from "../../hooks/useIntersection";
import { useEffect, useState } from "react";

const Stickynav = () => {
  const { ref: sectionNav, inView, entry } = useIntersection({});
  const [isScrollingUp, setIsScrollingUp] = useState(false);

  useEffect(() => {
    const checkScrollDir = (ev) => {
      if (ev.wheelDelta) {
        setIsScrollingUp(ev.wheelDelta > 0);
      }
      setIsScrollingUp(ev.deltaY < 0);
    };

    document.body.addEventListener("wheel", checkScrollDir);
    return () => {
      document.body.removeEventListener("wheel", checkScrollDir);
    };
  });

  return (
    <section ref={sectionNav}>
      <nav
        className={`${styles["nav"]} ${!inView ? styles["isFixed"] : ""} ${
          isScrollingUp ? styles["isPinned"] : styles["isUnpinned"]
        }
        ${inView ? styles["isPinned"] : ""}
        `}
      >
        <ul>
          <li style={{ display: "inline-block" }}>
            <ActiveLink href="/" activeClassName={styles["active--link"]}>
              <a>About</a>
            </ActiveLink>
          </li>
          <li>
            <ActiveLink href="/">
              <a>Jobs</a>
            </ActiveLink>
          </li>
        </ul>
      </nav>
    </section>
  );
};

export default Stickynav;
