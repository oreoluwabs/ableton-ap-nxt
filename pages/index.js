import Head from "next/head";
import ContentComponent from "../components/about/content";
import HeroComponent from "../components/about/hero";
import Stickynav from "../components/about/stickynav";
import Footer from "../components/footer";
import Header from "../components/header";

export default function Home() {
  return (
    <main className="container">
      <Head>
        <title>Ableton</title>
      </Head>
      <Header />
      <section
        style={{
          background: "#fff",
          marginTop: "3px",
          paddingBottom: "5.53vw",
        }}
      >
        <Stickynav />
        <HeroComponent />
        <ContentComponent />
      </section>
      {/* <Footer /> */}
    </main>
  );
}
